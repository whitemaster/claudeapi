# Развёртывание ClaudeAPI через docker-compose

## Установка зависимостей

Установите git и docker-compose на сервере:

```bash
sudo apt update
sudo apt install git docker-compose
```

## Клонирование репозитория

Склонируйте репозиторий проекта:

```bash 
git clone git@gitlab.com:whitemaster/claudeapi.git
```

## Настройка переменных окружения

Добавьте переменную окружения `COOKIE`:

```bash
export COOKIE="секретный ключ" 
```

## Запуск контейнеров

Перейдите в директорию проекта и запустите контейнеры:

```bash
cd claudeapi
docker-compose up -d
```

Приложение будет доступно по адресу http://server_ip