import os
from fastapi import FastAPI, File, UploadFile
from claude_api import Client

#cookie = os.environ.get('cookie')
cookie = "intercom-device-id-lupk8zyo=b83167c9-aa72-4e8c-97d3-91abb1f09255; __stripe_mid=614fb8e8-a9b9-440c-a46c-2749d4da2ef291785c; __ssid=ecc1cc9e649d94711c54844a25faf50; activitySessionId=b9f24379-9fb0-4c10-95c3-9af1303c3ce4; cf_clearance=BML92AnXtUBVYpDQB6ccQyJMejw0ly347SALv52x5dY-1700555640-0-1-fe0e6ed9.fa2be233.a90f3139-0.2.1700555640; __cf_bm=kKZ0PCrM66Bzd2mejWP3fzJtpA0tqIM_OgkYJ5_4QQ8-1700558540-0-AQtccjdpm79FaP307ynHDm7pi1R5C4vAfvIhzqBW6ujcgV0UCumdHmkeNSHKsSG+DE0/vJTWfKWPQJEEUY55RHI=; __stripe_sid=3d52835d-a07d-403d-a4d9-52615308c4a1ae0dc6; sessionKey=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; intercom-session-lupk8zyo=WnMvWWxSTzhseEF4bzh5b0l1NHFPTE5GRUNjeE9BejR2Qy9nakJyOHFNMVR0TUxGQnRrM3pYUTFvcUFzclQvOS0tQVo5UzBmWDNIb0liNW5PMXNNZUNpZz09--7ef7d5c60c8050a92d7455a2cb309c8b2cea934f"
claude_api = Client(cookie)

app = FastAPI()


@app.get("/health/")
def health_check():
    return "I'm alive"


@app.post("/chats/")
def create_chat():
    chat_id = claude_api.create_new_chat()['uuid']
    return {"chat_id": chat_id}


@app.post("/chats/send/")
def send_message(chat_id: str, message: str, filepath: str = None):
    if filepath:
        # Отправляем сообщение
        filepath = os.path.join("/uploads", filepath)
        response = claude_api.send_message(message, chat_id, attachment=filepath, timeout=600)
    else:
        response = claude_api.send_message(message, chat_id, timeout=600)

    return {"response": response}


@app.post("/files/")
def save_file(file: UploadFile = File(None)):
    # Получаем оригинальное имя файла
    filename = file.filename

    # Путь для сохранения файла
    filepath = os.path.join("/uploads", filename)

    # Создаем директорию, если нужно
    os.makedirs(os.path.dirname(filepath), exist_ok=True)

    # Сохраняем файл
    with open(filepath, "wb") as f:
        f.write(file.file.read())
    return {"filename": filename}


@app.get("/files/")
def show_files():
    # Указываем путь к директории
    directory = "/uploads"

    # Получаем список файлов
    files = os.listdir(directory)

    # Выводим список файлов
    return {"files": files}
